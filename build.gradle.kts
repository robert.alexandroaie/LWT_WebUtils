group = "lwt.lab"
version = "0.0.1"

plugins {
    java
    `java-library`
    war
}

tasks.wrapper {
    distributionType = Wrapper.DistributionType.ALL
    gradleVersion = "5.3.1"
    version = "5.3.1"
}

repositories {
    mavenCentral()
}

dependencies {
    api("javax.servlet:javax.servlet-api:4.0.1")
    api("org.apache.httpcomponents:httpcomponents-core:4.4.11")
}