package lwt.lab.utils;

/**
 * Small class that encapsulates how to construct a
 * search string for a particular search engine.
 */

public class SearchSpec {

	private final String name;
	private final String baseURL;

	SearchSpec(final String name,
			final String baseURL) {

		this.name = name;
		this.baseURL = baseURL;
	}

	/**
	 * Builds a URL for the results page by simply concatenating
	 * the base URL (http://...?someVar=") with the URL-encoded
	 * search string (jsp+training).
	 */

	String makeURL(final String searchString) {

		return (this.baseURL + searchString);
	}

	public String getName() {

		return (this.name);
	}
}
