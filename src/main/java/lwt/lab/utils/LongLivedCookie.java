package lwt.lab.utils;

import javax.servlet.http.Cookie;

/**
 * Cookie that persists 1 year. Default Cookie doesn't
 * persist past current browsing session.
 */

public class LongLivedCookie extends Cookie {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	private static final int SECONDS_PER_YEAR = 60 * 60 * 24 * 365;

	public LongLivedCookie(final String name, final String value) {

		super(name, value);
		this.setMaxAge(SECONDS_PER_YEAR);
	}
}
