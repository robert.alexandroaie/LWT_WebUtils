/**
 *
 */
package lwt.lab.utils;

/**
 * @author Robert
 *
 */
public class FilterUtils {

	private FilterUtils() {

	}

	/**
	 * Replaces characters that have special HTML meanings with their
	 * corresponding HTML character entities. Specifically, given a string, this
	 * method replaces all occurrences of {@literal
	 *  '<' with '&lt;', all occurrences of '>' with
	 *  '&gt;', and (to handle cases that occur inside attribute
	 *  values), all occurrences of double quotes with
	 *  '&quot;' and all occurrences of '&' with '&amp;'.
	 *  Without such filtering, an arbitrary string
	 *  could not safely be inserted in a Web page.
	 *  }
	 */
	public static String filter(final String input) {

		if (!hasSpecialChars(input)) {
			return (input);
		}
		final StringBuilder filtered = new StringBuilder(input.length());
		char c;
		for (int i = 0; i < input.length(); i++) {
			c = input.charAt(i);
			switch (c) {
			case '<':
				filtered.append("&lt;");
				break;
			case '>':
				filtered.append("&gt;");
				break;
			case '"':
				filtered.append("&quot;");
				break;
			case '&':
				filtered.append("&amp;");
				break;
			default:
				filtered.append(c);
			}
		}
		return (filtered.toString());
	}

	private static boolean hasSpecialChars(final String input) {

		boolean flag = false;
		if ((input != null) && (input.length() > 0)) {
			char c;
			for (int i = 0; i < input.length(); i++) {
				c = input.charAt(i);
				switch (c) {
				case '<':
					flag = true;
					break;
				case '>':
					flag = true;
					break;
				case '"':
					flag = true;
					break;
				case '&':
					flag = true;
					break;
				}
			}
		}
		return (flag);
	}

}
