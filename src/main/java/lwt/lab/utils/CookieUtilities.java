package lwt.lab.utils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

/**
 * Two static methods for use in cookie handling.
 */

public class CookieUtilities {

	public static final String VISITOR = "visitor";
	public static final String NEW_VISITOR = "new";
	public static final String REGULAR_VISITOR = "regular";

	/**
	 * Given the request object, a name, and a default value, this method tries
	 * to find the value of the cookie with the given name. If no cookie matches
	 * the name, the default value is returned.
	 */

	public static String getCookieValue(final HttpServletRequest request,
			final String cookieName, final String defaultValue) {

		final Cookie[] cookies = request.getCookies();
		if (cookies != null) {
			for (final Cookie cookie : cookies) {
				if (cookieName.equals(cookie.getName())) {
					return (cookie.getValue());
				}
			}
		}
		return (defaultValue);
	}

	/**
	 * Given the request object and a name, this method tries to find and return
	 * the cookie that has the given name. If no cookie matches the name, null
	 * is returned.
	 */

	public static Cookie getCookie(final HttpServletRequest request, final String cookieName) {

		final Cookie[] cookies = request.getCookies();
		if (cookies != null) {
			for (final Cookie cookie : cookies) {
				if (cookieName.equals(cookie.getName())) {
					return (cookie);
				}
			}
		}
		return (null);
	}
}
