package lwt.lab.html.utils;

import java.util.Map;

import lwt.lab.html.model.HTMLElement;
import lwt.lab.html.model.XMLElement;

public class HTMLElementBuilder {

	/**
	 *
	 */
	private HTMLElementBuilder() {

	}

	/**
	 * @param htmlElement
	 *
	 * @return
	 */
	public static String createElement(final HTMLElement htmlElement) {

		final StringBuilder builder = new StringBuilder();
		if (htmlElement != null) {
			startTag(htmlElement, builder);
			appendValue(htmlElement, builder);
			appendChildren(htmlElement, builder);
			closeTag(htmlElement, builder);
		}
		return builder.toString();
	}

	/**
	 * @param htmlElement
	 * @param builder
	 */
	private static void closeTag(final HTMLElement htmlElement, final StringBuilder builder) {

		if (htmlElement.isContainer()) {
			builder.append("</" + htmlElement.getTagName() + ">");
		} else {

			builder.append("/>");
		}
	}

	/**
	 * @param htmlElement
	 * @param builder
	 */
	private static void startTag(final HTMLElement htmlElement, final StringBuilder builder) {

		builder.append("<" + htmlElement.getTagName());
		appendAttributes(htmlElement.getAttributes(), builder);
		if (htmlElement.isContainer()) {
			builder.append(">");
		}
	}

	/**
	 * @param htmlElement
	 * @param builder
	 */
	private static void appendChildren(final HTMLElement htmlElement, final StringBuilder builder) {

		for (final XMLElement child : htmlElement.getChildren()) {
			HTMLElement htmlChild = null;
			if (child instanceof HTMLElement) {
				htmlChild = (HTMLElement) child;
				builder.append(createElement(htmlChild));
			}
		}
	}

	/**
	 * @param htmlElement
	 * @param builder
	 */
	private static void appendValue(final HTMLElement htmlElement, final StringBuilder builder) {

		if (htmlElement.isContainer()) {
			builder.append(htmlElement.getValue() != null ? htmlElement.getValue() : "");
		}
	}

	/**
	 * @param attributes
	 * @param builder
	 */
	private static void appendAttributes(final Map<String, String> attributes, final StringBuilder builder) {

		if (attributes != null && !attributes.isEmpty()) {
			for (final String attribute : attributes.keySet()) {
				builder.append(" " + attribute + "=\"" + attributes.get(attribute) + "\"");
			}
		}
	}
}
