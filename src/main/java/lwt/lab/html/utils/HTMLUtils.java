package lwt.lab.html.utils;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lwt.lab.html.model.A;
import lwt.lab.html.model.Body;
import lwt.lab.html.model.Form;
import lwt.lab.html.model.HTMLElement;
import lwt.lab.html.model.Head;
import lwt.lab.html.model.Html;
import lwt.lab.html.model.Li;
import lwt.lab.html.model.Link;
import lwt.lab.html.model.P;
import lwt.lab.html.model.Submit;
import lwt.lab.html.model.Table;
import lwt.lab.html.model.Td;
import lwt.lab.html.model.Textarea;
import lwt.lab.html.model.Th;
import lwt.lab.html.model.Title;
import lwt.lab.html.model.Ul;
import lwt.lab.html.model.XMLElement;

/**
 * Some simple time savers. Static methods.
 */

public class HTMLUtils {

	private HTMLUtils() {

	}

	private static String HTML5Doctype() {

		return "<!DOCTYPE html>\n";
	}

	public static String titledHTML(final String title) {

		return titleBodyStyleHTML(title, null, "../css/styles.css");
	}

	public static String cleanHTML() {

		return titleBodyStyleHTML(null, null, "../css/styles.css");
	}

	private static String titleBodyStyleHTML(final String title, final String body, final String cssPath) {

		final StringBuilder htmlBuilder = new StringBuilder();

		final Body bodyElement = new Body(body != null ? body : "");
		final Title titleElement = new Title(title != null ? title : "");
		final Link linkElement = new Link(cssPath);
		final Head headElement = new Head();
		headElement.addChild(titleElement);
		headElement.addChild(linkElement);
		final Html htmlElement = new Html();
		htmlElement.addChild(headElement);
		htmlElement.addChild(bodyElement);
		htmlBuilder.append(HTML5Doctype()).append(htmlElement.toHTML());
		return htmlBuilder.toString();
	}

	public static String titleBodyHTML(final String title, final String body) {

		return titleBodyStyleHTML(title, body, "./css/styles.css");
	}

	public static String createUL(final String[] data) {

		final Ul ulElement = new Ul();
		addChildren(data, ulElement, Li.class);
		return ulElement.toHTML();
	}

	private static void addChildren(final String[] data, final HTMLElement parent, final Class<?> childType) {

		for (final String value : data) {
			addChild(parent, value, childType);
		}
	}

	private static void addChild(final HTMLElement parent, final String elementData, final Class<?> childType) {

		final Constructor<?> constructor;
		try {
			constructor = childType.getConstructor(String.class);
			final XMLElement childElement = (XMLElement) constructor.newInstance(elementData);
			parent.addChild(childElement);
		} catch (final NoSuchMethodException | SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (final InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (final IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (final IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (final InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static String createTable(final String[][] data, final String[] headerLabels) {

		final List<XMLElement> ths = createThElements(headerLabels);
		final List<XMLElement> trs = createTrElements(data);

		final Table table = new Table();
		table.addChildren(ths);
		table.addChildren(trs);
		return table.toHTML();
	}

	private static List<XMLElement> createThElements(final String[] headerLabels) {

		final List<XMLElement> ths = new ArrayList<>();
		if (headerLabels != null) {
			for (int i = 0; i < headerLabels.length; i++) {
				ths.add(new Th(headerLabels[i]));
			}
		}
		return ths;
	}

	private static List<XMLElement> createTrElements(final String[][] data) {

		final List<XMLElement> trs = new ArrayList<>();
		List<XMLElement> tds;
		for (int i = 0; i < data.length; i++) {
			tds = new ArrayList<>();
			for (int j = 0; j < data[i].length; j++) {
				tds.add(new Td(data[i][j]));
			}
			trs.get(i).addChildren(trs);
		}
		return trs;
	}

	public static String createP(final String paragraph) {

		return new P(paragraph).toHTML();
	}

	public static String createA(final String content, final Map<String, String> attributes) {

		final A anchor = new A(content);
		anchor.addAttributes(attributes);
		return anchor.toHTML();
	}

	private static String createTextarea(final String content, final Map<String, String> attributes) {

		final Textarea textarea = new Textarea(content);
		textarea.addAttributes(attributes);
		return textarea.toHTML();
	}

	private static String createFrom(final String content, final Map<String, String> attributes) {

		final Form form = new Form(content);
		form.addAttributes(attributes);
		return form.toHTML();
	}

	private static String createSubmitButton(final String content, final Map<String, String> attributes) {

		final Submit submit = new Submit(content);
		submit.addAttributes(attributes);
		return submit.toHTML();
	}

	/**
	 * @return
	 */
	private static String createDefaultSubmitButton() {

		final Map<String, String> attributes = new HashMap<>();
		attributes.put("type", "submit");
		final String submitButton = HTMLUtils.createSubmitButton("Submit", attributes);
		return submitButton;
	}

	private static String createDefaultTextarea(final String textareaNameLabel) {

		final Map<String, String> attributes = new HashMap<>();
		attributes.put("rows", "6");
		attributes.put("cols", "40");
		attributes.put("name", textareaNameLabel);
		final String textarea = HTMLUtils.createTextarea("", attributes);
		return textarea;
	}

	public static String createTextareaForm(final String textareaNameLabel, final String action, final String method) {

		final String textarea = HTMLUtils.createDefaultTextarea(textareaNameLabel);
		final String submitButton = HTMLUtils.createDefaultSubmitButton();
		final Map<String, String> attributes = new HashMap<>();
		attributes.put("action", action);
		attributes.put("method", method);
		final String form = HTMLUtils.createFrom(textarea + submitButton, attributes);
		return form;
	}

	/**
	 * @param nameParamLabel
	 * @param surnameParamLabel
	 * @param emailParamLabel
	 * @param string
	 * @param string2
	 *
	 * @return
	 */
	public static String createPersonForm(final String nameParamLabel, final String surnameParamLabel, final String emailParamLabel,
			final String action, final String method) {

		final String nameInput = HTMLUtils.createLabeledTextInput("Name", nameParamLabel);
		final String surnameInput = HTMLUtils.createLabeledTextInput("Surame", surnameParamLabel);
		final String emailInput = HTMLUtils.createLabeledTextInput("Email", nameParamLabel);
		final String submitButton = HTMLUtils.createDefaultSubmitButton();
		final Map<String, String> attributes = new HashMap<>();
		attributes.put("action", action);
		attributes.put("method", method);
		final String form = HTMLUtils.createFrom(
				nameInput + "<br>" + surnameInput + "<br>" + emailInput + "<br>" + submitButton, attributes);
		return form;
	}

	/**
	 * @param string
	 * @param nameParamLabel
	 *
	 * @return
	 */
	private static String createLabeledTextInput(final String label, final String paramLabel) {

		final Map<String, String> attributes = new HashMap<>();
		attributes.put("type", "text");
		attributes.put("name", paramLabel);
		final String textarea = HTMLUtils.createLabeledInputText("", attributes);
		return textarea;
	}

	/**
	 * @param string
	 * @param attributes
	 *
	 * @return
	 */
	private static String createLabeledInputText(final String label, final Map<String, String> attributes) {

		return label + " " + HTMLElementBuilder.createElement(null);
	}

}
