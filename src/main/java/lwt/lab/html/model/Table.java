/**
 *
 */
package lwt.lab.html.model;

import static lwt.lab.html.HTMLKeywords.TABLE;

/**
 * @author Robert
 *
 */
public class Table extends GenericHTMLElement {

	public Table() {

		super();
		this.tagName = TABLE;
	}

	public Table(final String value) {

		this();
		this.value = value;
	}
}
