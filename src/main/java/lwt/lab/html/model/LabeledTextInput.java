/**
 *
 */
package lwt.lab.html.model;

/**
 * @author Robert
 *
 */
public class LabeledTextInput extends Text {

	private String label;

	/**
	 *
	 * @param label
	 */
	public LabeledTextInput(final String label) {

		super("");
		this.label = label;
	}

	/**
	 * @param name
	 * @param label
	 */
	public LabeledTextInput(final String name, final String label) {

		super(name);
		this.label = label;
	}

	/**
	 * @return the label
	 */
	public String getLabel() {

		return this.label;
	}

	/**
	 * @param label
	 *            the label to set
	 */
	public void setLabel(final String label) {

		this.label = label;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see lwt.lab.model.GenericHTMLElement#toHTML()
	 */
	@Override
	public String toHTML() {

		return this.label + " " + super.toHTML();
	}

}
