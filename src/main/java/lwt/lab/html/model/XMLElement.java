/**
 *
 */
package lwt.lab.html.model;

import java.util.Collection;
import java.util.Map;

/**
 * @author Robert
 *
 */
public interface XMLElement {

	Collection<XMLElement> getChildren();

	XMLElement getParent();

	String getTagName();

	boolean addChild(XMLElement child);

	String getValue();

	boolean addChildren(Collection<XMLElement> children);

	/**
	 *
	 * @return
	 */
	Map<String, String> getAttributes();

	/**
	 * @return
	 */
	boolean isContainer();

}
