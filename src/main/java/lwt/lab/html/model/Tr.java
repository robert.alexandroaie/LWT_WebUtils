/**
 *
 */
package lwt.lab.html.model;

import static lwt.lab.html.HTMLKeywords.TR;

/**
 * @author Robert
 *
 */
public class Tr extends GenericHTMLElement {

	private Tr() {

		super();
		this.tagName = TR;
	}

	public Tr(final String value) {

		this();
		this.value = value;
	}
}
