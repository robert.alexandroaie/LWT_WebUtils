/**
 *
 */
package lwt.lab.html.model;

import static lwt.lab.html.HTMLKeywords.P;

/**
 * @author Robert
 *
 */
public class P extends GenericHTMLElement {

	private P() {

		super();
		this.tagName = P;
	}

	public P(final String value) {

		this();
		this.value = value;
	}

}
