/**
 *
 */
package lwt.lab.html.model;

/**
 * @author Robert
 *
 */
public interface HTMLElement extends XMLElement {

	String toHTML();

}
