/**
 *
 */
package lwt.lab.html.model;

import static lwt.lab.html.HTMLKeywords.HREF;
import static lwt.lab.html.HTMLKeywords.LINK;
import static lwt.lab.html.HTMLKeywords.REL;
import static lwt.lab.html.HTMLKeywords.TYPE;

/**
 * @author Robert
 *
 */
public class Link extends GenericHTMLElement {

	/**
	 *
	 */
	private Link() {

		super();
		this.tagName = LINK;
		this.container = false;
		this.setRel("stylesheet");
		this.setType("text/css");
	}

	/**
	 * @param value
	 */
	public Link(final String cssPath) {

		this();
		this.setHref(cssPath);
	}

	private String setRel(final String rel) {

		return this.setAttribute(REL, rel);
	}

	private String setHref(final String href) {

		return this.setAttribute(HREF, href);
	}

	private String setType(final String type) {

		return this.setAttribute(TYPE, type);
	}

	public String getRel() {

		return this.getAttribute(REL);
	}

	public String getHref() {

		return this.getAttribute(HREF);
	}

	public String getType() {

		return this.getAttribute(TYPE);
	}
}
