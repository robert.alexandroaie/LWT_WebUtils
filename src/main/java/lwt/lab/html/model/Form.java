/**
 *
 */
package lwt.lab.html.model;

import static lwt.lab.html.HTMLKeywords.ACTION;
import static lwt.lab.html.HTMLKeywords.FORM;
import static lwt.lab.html.HTMLKeywords.METHOD;

/**
 * @author Robert
 *
 */
public class Form extends GenericHTMLElement {

	private Form() {

		super();
		this.tagName = FORM;
	}

	public Form(final String value) {

		this();
		this.value = value;
	}

	public String setAction(final String action) {

		return this.setAttribute(ACTION, action);
	}

	public String setMethod(final String method) {

		return this.setAttribute(METHOD, method);
	}

	public String getAction() {

		return this.getAttribute(ACTION);
	}

	public String getMethod() {

		return this.getAttribute(METHOD);
	}
}
