/**
 *
 */
package lwt.lab.html.model;

import static lwt.lab.html.HTMLKeywords.INPUT;
import static lwt.lab.html.HTMLKeywords.NAME;
import static lwt.lab.html.HTMLKeywords.TYPE;

/**
 * @author Robert
 *
 */
public class Input extends GenericHTMLElement {

	Input() {

		super();
		this.tagName = INPUT;
	}

	public Input(final String value) {

		this();
		this.value = value;
	}

	public String getType() {

		return this.getAttribute(TYPE);
	}

	public String getName() {

		return this.getAttribute(NAME);
	}

	String setType(final String type) {

		return this.setAttribute(TYPE, type);
	}

	String setName(final String name) {

		return this.setAttribute(NAME, name);
	}
}
