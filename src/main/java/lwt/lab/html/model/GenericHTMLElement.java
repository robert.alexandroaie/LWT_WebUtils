/**
 *
 */
package lwt.lab.html.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import lwt.lab.html.utils.HTMLElementBuilder;

/**
 * @author Robert
 *
 */
public abstract class GenericHTMLElement implements HTMLElement {

	protected String tagName;
	protected String value;
	private Map<String, String> attributes;
	private Collection<XMLElement> children;
	private HTMLElement parent;
	boolean container = true;

	/**
	 *
	 */
	public GenericHTMLElement() {

		this.attributes = new HashMap<>();
		this.children = new ArrayList<>();
	}

	public GenericHTMLElement(final String value) {

		this();
		this.value = value;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see lwt.lab.HTMLElement#toHTML()
	 */
	@Override
	public String toHTML() {

		return HTMLElementBuilder.createElement(this);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see lwt.lab.html.model.XMLElement#getTagName()
	 */
	@Override
	public String getTagName() {

		return this.tagName;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see lwt.lab.html.model.HTMLElement#addChild()
	 */
	@Override
	public boolean addChild(final XMLElement child) {

		boolean result = false;
		HTMLElement element = null;
		if (child instanceof HTMLElement) {
			element = (HTMLElement) child;
			result = this.children.add(element);
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see lwt.lab.html.model.HTMLElement#getAttributes()
	 */
	@Override
	public Map<String, String> getAttributes() {

		return this.attributes;
	}

	String getAttribute(final String name) {

		return this.attributes.get(name);
	}

	String setAttribute(final String name, final String value) {

		this.attributes.remove(name);
		return this.attributes.put(name, value);
	}

	/**
	 * @return the children
	 */
	@Override public Collection<XMLElement> getChildren() {

		return this.children;
	}

	/**
	 * @param children
	 *            the children to set
	 */
	public void setChildren(final Collection<XMLElement> children) {

		this.children = children;
	}

	/**
	 * @return the parent
	 */
	@Override public HTMLElement getParent() {

		return this.parent;
	}

	/**
	 * @param parent
	 *            the parent to set
	 */
	public void setParent(final HTMLElement parent) {

		this.parent = parent;
	}

	/**
	 * @return the value
	 */
	@Override
	public String getValue() {

		return this.value;
	}

	/**
	 * @param value
	 *            the value to set
	 */
	public void setValue(final String value) {

		this.value = value;
	}

	/**
	 * @param attributes
	 *            the attributes to set
	 */
	public void addAttributes(final Map<String, String> attributes) {

		for (final String key : attributes.keySet()) {
			this.setAttribute(key, attributes.get(key));
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see lwt.lab.html.model.HTMLElement#isContainable()
	 */
	@Override
	public boolean isContainer() {

		return this.container;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see lwt.lab.html.model.HTMLElement#addChildren(java.util.Collection)
	 */
	@Override
	public boolean addChildren(final Collection<XMLElement> children) {

		return this.children.addAll(children);
	}

}
