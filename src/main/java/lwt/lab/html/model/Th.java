/**
 *
 */
package lwt.lab.html.model;

import static lwt.lab.html.HTMLKeywords.TH;

/**
 * @author Robert
 *
 */
public class Th extends GenericHTMLElement {

	private Th() {

		super();
		this.tagName = TH;
	}

	public Th(final String value) {

		this();
		this.value = value;
	}

}
