/**
 *
 */
package lwt.lab.html.model;

import static lwt.lab.html.HTMLKeywords.TEXTAREA;

/**
 * @author Robert
 *
 */
public class Textarea extends Input {

	private Textarea() {

		super();
		this.setType(TEXTAREA);
	}

	public Textarea(final String value) {

		this();
		this.value = value;
	}

}
