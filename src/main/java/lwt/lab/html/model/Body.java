/**
 *
 */
package lwt.lab.html.model;

import static lwt.lab.html.HTMLKeywords.BODY;

/**
 * @author Robert
 *
 */
public class Body extends GenericHTMLElement {

	/**
	 *
	 */
	public Body() {

		super();
		this.tagName = BODY;
	}

	/**
	 *
	 * @param value
	 */
	public Body(String value) {

		this();
		this.value = value;
	}
}
