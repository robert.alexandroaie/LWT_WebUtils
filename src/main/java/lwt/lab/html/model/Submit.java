/**
 *
 */
package lwt.lab.html.model;

import static lwt.lab.html.HTMLKeywords.SUBMIT;

/**
 * @author Robert
 *
 */
public class Submit extends Input {

	private Submit() {

		super();
		this.setType(SUBMIT);
	}

	public Submit(final String value) {

		this();
		this.setValue(value);
	}
}
