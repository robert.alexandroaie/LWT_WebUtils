/**
 *
 */
package lwt.lab.html.model;

import static lwt.lab.html.HTMLKeywords.OL;

/**
 * @author Robert
 *
 */
public class Ol extends GenericHTMLElement {

	/**
	 *
	 */
	public Ol() {

		super();
		this.tagName = OL;
	}
}
